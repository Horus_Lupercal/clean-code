package com.foxminded.cleancode.integerdivision.ServiceTest;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.foxminded.cleancode.integerdivision.dto.IntegerDivisionDTO;
import com.foxminded.cleancode.integerdivision.service.FormatService;

public class FormatServiceTest {
	
	private FormatService formatService = new FormatService(); 
	
	@Test
	public void testFormatDividerCorrectDataValueAndExceptedIsOk() {
		IntegerDivisionDTO read = getIntegerDivisionCurrentData();
		List<String> realStringArray = Arrays.asList(formatService.formatDivider(read).split(System.lineSeparator()));
		
		List<String> exptedArray = Arrays.asList("_142536|22", 
				  " 132   |----",
				  " ---   |6478",
				  " _105  ",
				  "   88  ",
				  "   --  ",
				  "  _173 ",
				  "   154 ",
				  "   --- ",
				  "   _196",
				  "    176",
				  "    ---",
				  "     20");
		
		Assertions.assertEquals(exptedArray, realStringArray);
	}
	
	@Test
	public void testFormatDividerwithoutCountDataValueAndExceptedOnlyBase() {
		IntegerDivisionDTO read = getIntegerDivisionDataWithoutCount();
		List<String> realStringArray = Arrays.asList(formatService.formatDivider(read).split(System.lineSeparator()));
	
		List<String> exptedArray = Arrays.asList("_14|222", "   |-", "   |0");
		Assertions.assertEquals(exptedArray, realStringArray);
	}
	
	private IntegerDivisionDTO getIntegerDivisionCurrentData() {
		IntegerDivisionDTO integerDiv = new IntegerDivisionDTO();
		integerDiv.setDividend(142536);
		integerDiv.setDivider(22);
		integerDiv.setLastRest(20);
		integerDiv.getResults().addAll(Arrays.asList("6", "4", "7", "8"));
		integerDiv.getRests().addAll(Arrays.asList("142", "105", "173", "196"));
		integerDiv.getFactums().addAll(Arrays.asList("132", "88", "154", "176"));
		return integerDiv;	
	}
	
	private IntegerDivisionDTO getIntegerDivisionDataWithoutCount() {
		IntegerDivisionDTO integerDiv = new IntegerDivisionDTO();
		integerDiv.setDividend(14);
		integerDiv.setDivider(222);
		return integerDiv;	
	}
}
