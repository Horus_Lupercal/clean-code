package com.foxminded.cleancode.integerdivision.ServiceTest;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.foxminded.cleancode.integerdivision.service.DividerService;

public class DividerServiceTest {

	private DividerService dividerService = new DividerService();
	
	@Test 
	public void testDivideTheNumberCorrectValueAndExceptedIsOk() {
		String realString = dividerService.divideTheNumber(142536, 22);
		List<String> realStringArray =  Arrays.asList(realString.split(System.lineSeparator()));
		
		System.out.println(realString);
		List<String> exptedArray = Arrays.asList("_142536|22", 
							  " 132   |----",
							  " ---   |6478",
							  " _105  ",
							  "   88  ",
							  "   --  ",
							  "  _173 ",
							  "   154 ",
							  "   --- ",
							  "   _196",
							  "    176",
							  "    ---",
							  "     20");
				
		Assertions.assertEquals(exptedArray, realStringArray);
	}
		
	@Test
	public void testDivideTheNumberZeroDivideValueAndExceptedException() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {  
			 dividerService.divideTheNumber(254, 0);
		});
	}
	
	@Test
	public void testDivideTheNumberWithSmallerDividentValueAndExceptedOnlyBase() {
		List<String> realStringArray = Arrays.asList(dividerService.divideTheNumber(5, 25).split(System.lineSeparator()));
		List<String> exptedArray = Arrays.asList("_5|25", "  |-", "  |0");
		Assertions.assertEquals(exptedArray, realStringArray);
	}
}
