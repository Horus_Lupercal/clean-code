package com.foxminded.cleancode.anagram;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AnagramTest {

	private Anagram anagram = new Anagram();

	@Test
	public void testReverseLettersWhenMixedInputStringTheShoudBeExpectedLineResponce() {
		String correctString = "a1bcd efg!h";
		String expectedString = "d1cba hgf!e";
		String stringAfterReverse = anagram.reverseLetters(correctString);
		Assertions.assertEquals(stringAfterReverse, expectedString);
	}

	@Test
	public void testReverseLettersWhenOnlyDigitsInputStringTheShoudBeNonChangedResponce() {
		String correctString = "1234 1349";
		String stringAfterReverse = anagram.reverseLetters(correctString);
		Assertions.assertEquals(correctString, stringAfterReverse);
	}

	@Test
	public void testReverseLettersWhenOnlyLettersInputStringTheShoudBeExpectedLineResponce() {
		String correctString = "xyzqwerty";
		String expectedString = "ytrewqzyx";
		String stringAfterReverse = anagram.reverseLetters(correctString);
		Assertions.assertEquals(stringAfterReverse, expectedString);
	}

	@Test
	public void testReverseLettersWhenNullInputStringTheShoudBeNullResponse() {
		Assertions.assertNull(anagram.reverseLetters(null));
	}

	@Test
	public void testReverseLetterWhenEmptyLineInputStringTheShouldBeNullResponse() {
		Assertions.assertNull(anagram.reverseLetters(""));
	}
}