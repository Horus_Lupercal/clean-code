package com.foxminded.cleancode.integerdivision.service;

import java.util.List;

import com.foxminded.cleancode.integerdivision.dto.IntegerDivisionDTO;

public class DividerService {
	
	private FormatService formatService = new FormatService();

	public String divideTheNumber(Integer dividend, Integer divider) {
		checkValidation(dividend, divider);
		IntegerDivisionDTO integerDiv = new IntegerDivisionDTO();
		integerDiv.setDividend(dividend);
		integerDiv.setDivider(divider);

		String[] digits = dividend.toString().split("");
		countResult(digits, integerDiv);
		
		return formatService.formatDivider(integerDiv);
	}
	
	private void countResult(String[] digits, IntegerDivisionDTO integerDivisionDTO) {
		StringBuilder number = new StringBuilder();
		for (String digit : digits) {
			number.append(digit);
			Integer currentDivided = Integer.parseInt(number.toString());
			
			if (currentDivided >= integerDivisionDTO.getDivider()) {
				integerDivisionDTO.getRests().add(number.toString());
				Integer result = currentDivided / integerDivisionDTO.getDivider();
				integerDivisionDTO.getResults().add(result.toString());
				Integer factum = integerDivisionDTO.getDivider() * getLastResult(integerDivisionDTO.getResults());
				integerDivisionDTO.getFactums().add(factum.toString());
				int rest = currentDivided - factum;
			
				number.setLength(0);
				if (rest != 0) {
					number.append(rest);
				}
				integerDivisionDTO.setLastRest(rest);
			} else if (!integerDivisionDTO.getResults().isEmpty()) {
				integerDivisionDTO.getResults().add("0");
				integerDivisionDTO.setLastRest(Integer.parseInt(digit));
			}
		}
	}
	
	private void checkValidation(Integer dividend, Integer divider) {
		if (dividend == null || divider == null || divider == 0) {
			throw new IllegalArgumentException();
		}
	}
	
	private int getLastResult(List<String> results) {
		return Integer.parseInt(results.get(results.size() - 1));
	}
}
