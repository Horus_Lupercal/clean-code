package com.foxminded.cleancode.integerdivision.service;

import com.foxminded.cleancode.integerdivision.dto.IntegerDivisionDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class FormatService {

	public String formatDivider(IntegerDivisionDTO integerDivisionDTO) {
		StringBuilder result = new StringBuilder("_");
		result.append(buildBase(integerDivisionDTO));
		if (integerDivisionDTO.getDividend() > integerDivisionDTO.getDivider()) {
			result.append(System.lineSeparator());
			result.append(buildTail(integerDivisionDTO));
		}
		return result.toString();
	}

	private String buildTail(IntegerDivisionDTO integerDivisionDTO) {
		List<String> tail = new ArrayList<>();
		int dividendLength = integerDivisionDTO.getDividend().toString().length();
		int factumSize = integerDivisionDTO.getFactums().size();
		int restSize = integerDivisionDTO.getRests().size();
		int factumIndex = 1;
		int restIndex = 1;

		int offset = 0;

		while (factumIndex < factumSize && restIndex < restSize) {
			String rest = integerDivisionDTO.getRests().get(restIndex);
			String factum = integerDivisionDTO.getFactums().get(factumIndex);
			String previousRest = integerDivisionDTO.getRests().get(restIndex - 1);
			String previousFactums = integerDivisionDTO.getFactums().get(factumIndex - 1);
			int digitsCount = countRestDigitsSize(previousRest, previousFactums);
			
			offset+=(previousRest.length() - digitsCount);
			
			tail.add(buildString(dividendLength, rest, offset, true));
			restIndex++;

			int countOffSetForNonRestString = offset + rest.length() - factum.length();

			tail.add(buildString(dividendLength, factum, countOffSetForNonRestString, false));
			factumIndex++;

			tail.add(buildString(dividendLength, repeat(factum.length(), "-"), countOffSetForNonRestString, false));
		}

		tail.add(buildString(dividendLength, integerDivisionDTO.getLastRest().toString(),
				dividendLength - integerDivisionDTO.getLastRest().toString().length(), false));

		return listToString(tail, true);
	}

	private int countRestDigitsSize(String priviusRest, String currectFactum) {
		int factum = Integer.parseInt(currectFactum);
		int rest = Integer.parseInt(priviusRest);
		Integer result = rest - factum;
		return result == 0 ? 0 : result.toString().length();

	}

	private String buildBase(IntegerDivisionDTO integerDiv) {
		List<String> base = new ArrayList<>(3);
		int dividendLength = integerDiv.getDividend().toString().length();
		String firstFactum = "";
		String firstRest = "";
		String result = "0";
		if (!integerDiv.getRests().isEmpty() && !integerDiv.getFactums().isEmpty()) {
			firstFactum = integerDiv.getFactums().get(0);
			firstRest = integerDiv.getRests().get(0);
			result = listToString(integerDiv.getResults(), false);
			}
		String dashSecondLine = repeat(result.length(), "-");
		String dashThirdLine = repeat(firstFactum.length(), "-");
		
		base.add(integerDiv.getDividend() + "|" + integerDiv.getDivider());

		base.add(buildString(dividendLength, firstFactum, firstRest.length() - firstFactum.length(), false) + "|"
				+ dashSecondLine);

		base.add(
				buildString(dividendLength, dashThirdLine, firstRest.length() - firstFactum.length(), false) + "|" + result);

		return listToString(base, true);
	}

	private String buildString(int dividendLength, String value, int offset, boolean isRest) {
		if (isRest) {
			value = "_" + value;
		} else {
			offset++;
		}
		String leftSide = repeat(offset, " ");
		int countRightSpace = dividendLength + 2 - (leftSide.length() + value.length());
		String rightSide = repeat(countRightSpace - 1, " ");
		return leftSide + value + rightSide;
	}

	private String repeat(int count, String repeatString) {
		StringBuilder resultString = new StringBuilder();
		for (int i = 0; i < count; i++) {
			resultString.append(repeatString);
		}
		return resultString.toString();
	}

	private String listToString(List<String> results, boolean withLineSeparator) {
		StringJoiner result = new StringJoiner(withLineSeparator ? System.lineSeparator() : "");
		for (String digit : results) {
			result.add(digit);
		}
		return result.toString();
	}
}
