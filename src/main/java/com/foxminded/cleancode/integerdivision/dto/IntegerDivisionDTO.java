package com.foxminded.cleancode.integerdivision.dto;

import java.util.ArrayList;
import java.util.List;

public class IntegerDivisionDTO {

	private Integer divider;
	private Integer dividend;
	private Integer lastRest;
	private List<String> results = new ArrayList<>();
	private List<String> rests = new ArrayList<>();
	private List<String> factums = new ArrayList<>();
	
	public Integer getLastRest() {
		return lastRest;
	}
	
	public void setLastRest(Integer lastRest) {
		this.lastRest = lastRest;
	}
	
	public void setDividend(Integer dividend) {
		this.dividend = dividend;
	}

	public Integer getDividend() {
		return dividend;
	}

	public void setDivider(Integer divider) {
		this.divider = divider;
	}

	public Integer getDivider() {
		return divider;
	}

	public List<String> getResults() {
		return results;
	}

	public List<String> getRests() {
		return rests;
	}

	public List<String> getFactums() {
		return factums;
	}
	
	@Override
	public String toString() {
		return String.format("Divident is: %d, divider is: %d, lastRest is: %d. Result = %s. Rest = %s. Factums = %s",
				dividend, divider, lastRest, String.valueOf(results), String.valueOf(rests), String.valueOf(factums));
	}
}
