package com.foxminded.cleancode.integerdivision;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.foxminded.cleancode.integerdivision.service.DividerService;

public class LongDevisionApplication {

	public static void main(String[] args) {
		int divider = 0;
		int dividend = 0;
		
		try (InputStreamReader input = new InputStreamReader(System.in);
				BufferedReader reader = new BufferedReader(input)) {
			System.out.println("please enter the dividend:");
			dividend = Integer.parseInt(reader.readLine());
			System.out.println("please enter the divider:");
			divider = Integer.parseInt(reader.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		DividerService dividerService = new DividerService();
		System.out.println(dividerService.divideTheNumber(dividend, divider));
	}
}
