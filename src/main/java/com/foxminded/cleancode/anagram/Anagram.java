package com.foxminded.cleancode.anagram;

import java.util.StringJoiner;

public class Anagram {

	public String reverseLetters(String line) {
		if (line == null || line.isEmpty()) {
			return null;
		}
		String[] words = line.split("\\s");
		StringJoiner anagramString = new StringJoiner(" ");
		for (int i = 0; i < words.length; i++) {
			anagramString.add((reverseWord(words[i])));
		}
		return anagramString.toString();
	}

	private String reverseWord(String word) {
		char[] chars = word.toCharArray();
		int indexOfLastCharacter = chars.length - 1;
		int indexOfFirstCharacter = 0;
		while (indexOfFirstCharacter < indexOfLastCharacter) {
			if (!Character.isAlphabetic(chars[indexOfFirstCharacter])) {
				indexOfFirstCharacter++;
			}
			if (!Character.isAlphabetic(chars[indexOfLastCharacter])) {
				indexOfLastCharacter--;
			}
			if (Character.isAlphabetic(chars[indexOfFirstCharacter])
					&& Character.isAlphabetic(chars[indexOfLastCharacter])) {
				char temp = chars[indexOfFirstCharacter];
				chars[indexOfFirstCharacter] = chars[indexOfLastCharacter];
				chars[indexOfLastCharacter] = temp;
				indexOfFirstCharacter++;
				indexOfLastCharacter--;
			}
		}
		return String.valueOf(chars);
	}
}
